[x] create a new app called accounts
[x] install the accounts app into installed apps
[x] register the view in your urlpatterns list with the path "login/" and the name "login"
[x] include accounts urls in expenses urls with path("accounts/") 
[x] make a templates directory in our accounts app
[x] add a resgistration directory as a child of templates
[x] create an html template named login.html in our child registration directory
[ ] create a base.html for template inheritance
[x] <form method="post"> in our login.html + required html5, 
[x] create and set LOGIN_REDIRECT_URL in our settings.py and add the value "home"
[x] create django app called receipts
[x] install the app
[x] create a template and child folder for receipts
[x] create a list.html
[x] create a class view in the receipts app named ReceiptListView
[x] template_name should be list.html
[x] register the list view in the receipt app's urls.py in the urlpatterns list with the "" and the name "home"
[x] in expenses/urls.py indlude the receipts' urls/py module with the path receipts/
[x] import the redirectview and register it with this code to redirect local host to the home path