from django.urls import path
from receipts.views import (
    AccountListView,
    ReceiptListView,
    ReceiptCreateView,
    AccountCreateView,
    ExpenseCategoryCreateView,
    ExpenseCategoryListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="create_receipt_account",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_expense_category",
    ),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="list_categories",
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
]
